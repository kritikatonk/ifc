var Gallery = function () {
  return {
    init: function () {
      var prevScrollPos = $(window).scrollTop();
      var navbar = $("#navbar");
      $(window).scroll(function () {
        var currentScrollPos = $(window).scrollTop();
        if (prevScrollPos > currentScrollPos) {
          navbar.removeClass("hide");
        } else {
          navbar.addClass("hide");
          $('.navbar-collapse').collapse('hide');
        }
        prevScrollPos = currentScrollPos;
      });

      var container = document.querySelector(".container-fluid");
      fetch("/static/data/gallery.json")
        .then(function (response) {
          return response.json();
        })
        .then(function (galleryData) {
          const years = Object.keys(galleryData).reverse();
          for (var year of years) {
            var headerRow = document.createElement("div");
            headerRow.className = "row my-2";
            var headerColumn = document.createElement("div");
            headerColumn.className = "col-12 stretched-column text-center";
            var yearText = document.createElement("h2");
            yearText.textContent = year;

            var anchor = document.createElement("a");
            anchor.href = `/years/${year}.html`;
            anchor.appendChild(yearText);

            headerColumn.appendChild(anchor);
            headerRow.appendChild(headerColumn);
            container.appendChild(headerRow);

            // Create images row
            var imagesRow = document.createElement("div");
            imagesRow.className = "my-2 slider";

            var images = galleryData[year];
            for (var i = 0; i < images.length; i++) {
              var imageColumn = document.createElement("div");
              imageColumn.className = "marquee-container";

              var card = document.createElement("div");
              card.className = "card my-2 mx-2";
              card.id = `${year}`;

              var img = document.createElement("img");
              img.id = `${year}`;
              img.src = images[i][1];
              img.alt = images[i][0];
              img.className = "card-img-top";

              var caption = document.createElement("div");
              caption.className = "card-body text-center";
              caption.textContent = images[i][0];

              card.appendChild(img);
              card.appendChild(caption);
              imageColumn.appendChild(card);

              // Add click event to show modal
              card.addEventListener("click", function () {
                var year = this.id;
                window.location.href = `/years/${year}.html`;
              });

              img.addEventListener("click", function () {
                var year = this.id;
                window.redirect = `/years/${year}.html`;
              });

              imagesRow.appendChild(imageColumn);
            }
            container.appendChild(imagesRow);
          }
        })
        .catch(function (error) {
          console.log("Error fetching JSON data:", error);
        });
    }
  };
}();

jQuery(document).ready(function () {
  Gallery.init();
});
