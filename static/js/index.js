document.addEventListener("DOMContentLoaded", function () {
  var boxes = document.getElementsByClassName("box");
  for (var i = 0; i < boxes.length; i++) {
    boxes[i].classList.add("fade-in");
  }
});

console.log(navigator.userAgent);
if (!/iPhone/i.test(navigator.userAgent)) {
  let bg = document.getElementById("bg");
  let mountain = document.getElementById("mountain");
  let road = document.getElementById("road");
  let text = document.getElementById("text");
  console.log(navigator.userAgent);
  window.addEventListener('scroll', function () {
    var value = window.scrollY;
    mountain.style.top = -value * 0.15 + 'px';
    road.style.top = value * 0.15 + 'px';
    text.style.top = value * 1 + 'px';
  });
}

$(document).scroll(function () {
  var scroll = $(window).scrollTop();
  var amount = -160 + (scroll * 0.8);
  if (amount < 10) {
    $('.letter').css({ left: amount + "px" });
  }
});

var textWrapper = document.querySelector('.ml11 .letters');
textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

anime.timeline({ loop: true })
  .add({
    targets: '.ml11 .line',
    scaleY: [0, 1],
    opacity: [0.5, 1],
    easing: "easeOutExpo",
    duration: 700
  })
  .add({
    targets: '.ml11 .line',
    translateX: [0, document.querySelector('.ml11 .letters').getBoundingClientRect().width + 10],
    easing: "easeOutExpo",
    duration: 700,
    delay: 100
  }).add({
    targets: '.ml11 .letter',
    opacity: [0, 1],
    easing: "easeOutExpo",
    duration: 600,
    offset: '-=775',
    delay: (el, i) => 34 * (i + 1)
  }).add({
    targets: '.ml11',
    opacity: 0,
    duration: 1000,
    easing: "easeOutExpo",
    delay: 1000
  });

var textWrapper = document.querySelector('.ml14 .letters');
textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

anime.timeline({ loop: true })
  .add({
    targets: '.ml14 .line',
    scaleX: [0, 1],
    opacity: [0.5, 1],
    easing: "easeInOutExpo",
    duration: 900
  }).add({
    targets: '.ml14 .letter',
    opacity: [0, 1],
    translateX: [40, 0],
    translateZ: 0,
    scaleX: [0.3, 1],
    easing: "easeOutExpo",
    duration: 800,
    offset: '-=600',
    delay: (el, i) => 150 + 25 * i
  }).add({
    targets: '.ml14',
    opacity: 0,
    duration: 1000,
    easing: "easeOutExpo",
    delay: 1000
  });

const counters = document.querySelectorAll('.value');
const speed = 1000000000;

counters.forEach(counter => {
  const animate = () => {
    const value = +counter.getAttribute('akhi');
    const data = +counter.innerText;
    const time = value / speed;
    if (data < value) {
      counter.innerText = Math.ceil(data + time);
      setTimeout(animate, 1);
    } else {
      counter.innerText = value;
    }
  }
  animate();
});

var textWrapper = document.querySelector('.ml12');
textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

anime.timeline({ loop: true })
  .add({
    targets: '.ml12 .letter',
    translateX: [40, 0],
    translateZ: 0,
    opacity: [0, 1],
    easing: "easeOutExpo",
    duration: 1200,
    delay: (el, i) => 500 + 30 * i
  }).add({
    targets: '.ml12 .letter',
    translateX: [0, -30],
    opacity: [1, 0],
    easing: "easeInExpo",
    duration: 1100,
    delay: (el, i) => 100 + 30 * i
  });

// Wrap every letter in a span
var textWrapper = document.querySelector('.ml10 .letters');
textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

anime.timeline({ loop: true })
  .add({
    targets: '.ml10 .letter',
    rotateY: [-90, 0],
    duration: 1300,
    delay: (el, i) => 45 * i
  }).add({
    targets: '.ml10',
    opacity: 0,
    duration: 1000,
    easing: "easeOutExpo",
    delay: 1000
  });


const isPhone = window.matchMedia("(max-width: 992px)").matches;
const isIPhone = window.matchMedia("(max-width: 992px)").matches && /iPhone/i.test(navigator.userAgent);
function toggleElements() {
  const allElement = document.getElementById("all");
  const iphoneElement = document.getElementById("iphone");

  if (isPhone) {
    allElement.hidden = true;
    iphoneElement.hidden = false;
  } else {
    allElement.hidden = false;
    iphoneElement.hidden = true;
  }
  if (isIPhone === false) {
    var prevScrollPos = $(window).scrollTop();
    var navbar = $("#navbar");
    $(window).scroll(function () {
      var currentScrollPos = $(window).scrollTop();
      if (prevScrollPos > currentScrollPos) {
        navbar.removeClass("hide");
      } else {
        navbar.addClass("hide");
        $('.navbar-collapse').collapse('hide');
      }
      prevScrollPos = currentScrollPos;
    });
  } else {
    console.log("iPhone!")
  }
}

toggleElements();
window.addEventListener("resize", toggleElements);