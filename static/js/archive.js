var Archive = function () {
  return {
    init: function () {

      $(window).scroll(function () {
        var scrollPos = $(document).scrollTop();
        var windowHeight = $(window).height();
        var documentHeight = $(document).height();

        if (scrollPos + windowHeight >= documentHeight) {
          $('#myFooter').removeClass('d-none');
        } else {
          $('#myFooter').addClass('d-none');
        }
      });

      var prevScrollPos = $(window).scrollTop();
      var navbar = $("#navbar");
      $(window).scroll(function () {
        var currentScrollPos = $(window).scrollTop();
        if (prevScrollPos > currentScrollPos) {
          navbar.removeClass("hide");
        } else {
          navbar.addClass("hide");
          $('.navbar-collapse').collapse('hide');
        }
        prevScrollPos = currentScrollPos;
      });

      var filter = 'all'; // Initial filter value
      function fetchData(category) {
        return fetch('/static/data/filter.json')
          .then(response => response.json())
          .then(data => {
            var year = $('#year').text();
            data = data[year];
            if (category === 'all') {
              return Object.values(data).flatMap(Object.values);
            } else {
              return data[category] || [];
            }
          })
          .catch(error => {
            console.error('Error:', error);
            return [];
          });
      }

      function populateGallery(items) {
        for (var i = 0; i < items.length; i++) {
          var caption = items[i][0];
          var imagePath = items[i][1];
          var category = items[i][2];

          if (i % 4 === 0 || i === items.length) {
            row = $('<div>').addClass('row');
            $('#gallery').append(row);
          }

          var card = $('<div>').addClass('card mx-auto my-1').attr('category', category);
          var cardImage = $('<img>').addClass('card-img-top').attr('src', imagePath).attr('alt', caption).attr('data-index', i);
          var cardBody = $('<div>').addClass('card-body');
          var cardCaption = $('<h6>').addClass('card-title caption-center text-center').text(caption);
          cardBody.append(cardCaption);
          card.append(cardImage).append(cardBody);
          row.append(card);
        }

        $('.card-img-top').click(function () {
          currentImageIndex = parseInt($(this).attr('data-index'));
          var imageModal = $('#imageModal');
          imageModal.find('.carousel-indicators').html('');
          imageModal.find('.carousel-inner').html('');

          for (var j = 0; j < items.length; j++) {
            var indicator = $('<li>').attr('data-target', '#imageCarousel').attr('data-slide-to', j);
            if (j === currentImageIndex) {
              indicator.addClass('active');
            }
            imageModal.find('.carousel-indicators').append(indicator);

            var carouselImage = $('<div>').addClass('carousel-item');
            if (j === currentImageIndex) {
              carouselImage.addClass('active');
            }
            var image = $('<img>').addClass('d-block w-100').attr('src', items[j][1]).attr('alt', items[j][0]);
            carouselImage.append(image);
            imageModal.find('.carousel-inner').append(carouselImage);
          }

          imageModal.modal('show');
          imageModal.find('.carousel-item').first().addClass('active');
          imageModal.addClass('modal-dark-overlay');
        });
      }

      function updateGallery() {
        fetchData(filter)
          .then(items => {
            $('#gallery').empty();
            populateGallery(items);
          })
          .catch(error => {
            console.error('Error:', error);
          });
      }

      fetch('/static/data/filter.json')
        .then(response => response.json())
        .then(data => {
          var year = $('#year').text();
          var categoryItems = data[year];
          var items = [];
          for (key in categoryItems) {
            var category = categoryItems[key];
            for (let i = 0; i < category.length; i++) {
              items.push(category[i]);
            }
          }
          populateGallery(items);
        })
        .catch(error => {
          console.error('Error:', error);
        });

      $('#filterButtons button').click(function () {
        $('#filterButtons button').removeClass('active');
        $(this).addClass('active');
        filter = $(this).attr('data-filter');

        updateGallery(filter);
      });
    }

  };
}();

jQuery(document).ready(function () {
  Archive.init();
});
